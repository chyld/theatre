package com.chyld.controllers;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.junit.Assert.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(value = {"/prepare-db.sql"})
public class MovieControllerTest {
    @Before
    public void setUp() throws Exception {
        RestAssured.port = 8001;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    // GET /api/movies
    public void shouldGetAllMoviesOnPage0() throws Exception {
        given().log().all().and().given().
            get("/api/movies")
            .then().log().all()
            .statusCode(200)
            .body(
                    "numberOfElements", is(3),
                    "totalPages", is(2),
                    "content[0].name", equalTo("The Matrix")
            );
    }

    @Test
    // POST /api/movies
    public void shouldCreateNewStudio() throws Exception {
        Map<String, Object> json = new HashMap<>();
        json.put("name", "Mission Impossible");
        json.put("released", "1989-01-16");
        json.put("genre", "ACTION");
        json.put("rating", "PG");
        json.put("studio", 3);

        given().log().all().and().given().
            contentType(ContentType.JSON).
            body(json).
            post("/api/movies")
            .then().log().all()
            .statusCode(200)
            .body(
                    "name", equalTo("Mission Impossible"),
                    "id", is(7),
                    "version", is(0),
                    "createdAt", greaterThan(1L),
                    "updatedAt", greaterThan(1L),
                    "studio.id", is(3)
            );
    }
}
